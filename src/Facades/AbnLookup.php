<?php

namespace SparkEleven\AbnLookup\Facades;

/**
 * Facade helper for AbnLookupService.
 *
 * @author Theodore Yaosin <theo@sparkeleven.com.au>
 *
 * @method static void reset(string $auth_guid = null, string $wsdl = null, int $wsdl_cache = null)
 * @method static string getAuthGuid()
 * @method static string getWsdl()
 * @method static int getWsdlCache()
 * @method static array searchByAbn(string $abn, bool $historical = false)
 * @method static array searchByAsic(string $acn, bool $historical = false)
 * @method static array searchByName(string $name, string $postcode = null, bool $isLegalName = null, bool $isTradingName = null, array $states = [])
 *
 * @see \SparkEleven\AbnLookup\Services\AbnLookupService
 */
class AbnLookup extends \Illuminate\Support\Facades\Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return self::class;
    }
}
