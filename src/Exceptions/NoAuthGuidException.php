<?php

namespace SparkEleven\AbnLookup\Exceptions;

use Exception;
use Symfony\Component\HttpFoundation\Response;

/**
 * Excpetion thrown when authentication GUID is not provided.
 *
 * @author Theodore Yaosin <theo@sparkeleven.com.au>
 */
class NoAuthGuidException extends Exception
{
    /**
     * Report the exception.
     *
     * @return void
     */
    public function report()
    {
        //
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        //
    }
}
