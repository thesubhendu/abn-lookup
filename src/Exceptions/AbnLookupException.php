<?php

namespace SparkEleven\AbnLookup\Exceptions;

use Exception;
use Symfony\Component\HttpFoundation\Response;

/**
 * Excpetion thrown when an exception received from ABN Lookup SOAP API.
 *
 * @author Theodore Yaosin <theo@sparkeleven.com.au>
 */
class AbnLookupException extends Exception
{
    /**
     * Exception object.
     *
     * @var \stdClass
     */
    protected $error;

    /**
     * Constructor.
     *
     * @param  \stdClass  $exception
     */
    public function __construct(\stdClass $exception)
    {
        $this->error = $exception;
    }

    /**
     * Report the exception.
     *
     * @return void
     */
    public function report()
    {
        //
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        //
    }

    /**
     * Get error from ABN Lookup API.
     *
     * @return \stdClass
     */
    public function error()
    {
        return fromObjectToArray($this->error);
    }
}
