<?php

if (! function_exists('toss')) {
    /**
     * Throw given exception.
     *
     * @param  \Exception  $exception
     * @return void
     * @throws \Exception
     */
    function toss(\Exception $exception): void
    {
        throw $exception;
    }
}

if (! function_exists('fromObjectToArray')) {
    /**
     * Converts a stdClass object to an array.
     *
     * @param  \stdClass  $object
     * @return array
     */
    function fromObjectToArray(\stdClass $object): array
    {
        return json_decode(json_encode($object), true);
    }
}

if (! function_exists('ynbool')) {
    /**
     * Converts a boolean value to string 'Y' or 'N'.
     *
     * @param  mixed  $value
     * @return string
     */
    function ynbool($value): string
    {
        return $value ? 'Y' : 'N';
    }
}
