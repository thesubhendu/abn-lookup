<?php

namespace Tests\Unit\Services;

use Tests\BaseTestCase as TestCase;

use SparkEleven\AbnLookup\Services\AbnLookupService;

/**
 * Unit testing for AbnLookupService.
 *
 *  WARNING: Test dummy data used in this test is historical, which is fetched
 *            in 06/12/2020.
 *
 * @author Theodore Yaosin <theo@sparkeleven.com.au>
 */
class AbnLookupServiceTest extends TestCase
{
    /**
     * AbnLookupService instance.
     *
     * @var \SparkEleven\AbnLookup\Services\AbnLookupService
     */
    protected $service;

    /**
     * Set up default configuration.
     *
     * @param  \Illuminate\Foundation\Application  $app
     */
    protected function useDefaultConfig($app)
    {
        // Setup default configuration
        $app->config->set('abn-lookup.auth_guid', '130aad5b-1613-4ec3-ae02-a3a431269320');
        $app->config->set(
            'abn-lookup.wsdl',
            \SparkEleven\AbnLookup\Services\AbnLookupService::DEFAULT_WSDL
        );
        $app->config->set('abn-lookup.wsdl_cache', WSDL_CACHE_DISK);
    }

    /**
     * Set config of authentication GUID as null.
     *
     * @param  \Illuminate\Foundation\Application  $app
     */
    protected function useAuthGuidNull($app)
    {
        $app->config->set(
            'abn-lookup.auth_guid',
            null
        );
    }

    /**
     * Set config of authentication GUID as wrong value.
     *
     * @param  \Illuminate\Foundation\Application  $app
     */
    protected function useAuthGuidIncorrect($app)
    {
        $app->config->set(
            'abn-lookup.auth_guid',
            'Wrong Auth Guid'
        );
    }

    /**
     * Set config of WSDL path as wrong value.
     *
     * @param  \Illuminate\Foundation\Application  $app
     */
    protected function useWsdlIncorrect($app)
    {
        $app->config->set(
            'abn-lookup.wsdl',
            'wrongwsdl'
        );
    }

    /**
     * Reset AbnLookupService instance.
     */
    protected function resetService()
    {
        AbnLookupService::reset(
            $this->app->config['abn-lookup.auth_guid'],
            $this->app->config['abn-lookup.wsdl'],
            $this->app->config['abn-lookup.wsdl_cache']
        );
    }

    /**
     * Test if service is initialized correctly.
     */
    public function testServiceInitialization()
    {
        $this->useDefaultConfig($this->app);
        $this->resetService();

        $this->assertEquals(
            $this->app->config['abn-lookup.auth_guid'],
            AbnLookupService::getAuthGuid()
        );

        $this->assertEquals(
            $this->app->config['abn-lookup.wsdl'],
            AbnLookupService::getWsdl()
        );

        $this->assertEquals(
            $this->app->config['abn-lookup.wsdl_cache'],
            AbnLookupService::getWsdlCache()
        );
    }

    /**
     * Test searchByAbn method.
     */
    public function testSearchByAbn()
    {
        $this->useDefaultConfig($this->app);
        $this->resetService();

        $this->assertEquals(AbnLookupService::searchByAbn('41 726 564 339'), [
            "recordLastUpdatedDate" => "2017-05-04",
            "ABN" => [
                "identifierValue" => "41726564339",
                "isCurrentIndicator" => "Y",
                "replacedFrom" => "0001-01-01",
            ],
            "entityStatus" => [
                "entityStatusCode" => "Active",
                "effectiveFrom" => "2012-02-13",
                "effectiveTo" => "0001-01-01",
            ],
            "ASICNumber" => "",
            "entityType" => [
                "entityTypeCode" => "IND",
                "entityDescription" => "Individual/Sole Trader",
            ],
            "legalName" => [
                "givenName" => "MATTHEW",
                "otherGivenName" => "",
                "familyName" => "GLOVER",
                "effectiveFrom" => "2013-03-21",
                "effectiveTo" => "0001-01-01",
            ],
            "mainBusinessPhysicalAddress" => [
                "stateCode" => "VIC",
                "postcode" => "3199",
                "effectiveFrom" => "2014-09-17",
                "effectiveTo" => "0001-01-01",
            ],
            "businessName" => [
                "organisationName" => "Spark Eleven",
                "effectiveFrom" => "2014-08-14",
            ],
        ]);

        $this->assertEquals(AbnLookupService::searchByAbn('15 165 912 970'), [
            "recordLastUpdatedDate" => "2020-03-20",
            "ABN" => [
                "identifierValue" => "15165912970",
                "isCurrentIndicator" => "Y",
                "replacedFrom" => "0001-01-01",
            ],
            "entityStatus" => [
                "entityStatusCode" => "Active",
                "effectiveFrom" => "2013-09-23",
                "effectiveTo" => "0001-01-01",
            ],
            "ASICNumber" => "165912970",
            "entityType" => [
                "entityTypeCode" => "PRV",
                "entityDescription" => "Australian Private Company",
            ],
            "mainName" => [
                "organisationName" => "MUSINDIE PTY. LTD.",
                "effectiveFrom" => "2013-09-23",
            ],
            "mainBusinessPhysicalAddress" => [
                "stateCode" => "VIC",
                "postcode" => "3103",
                "effectiveFrom" => "2017-02-27",
                "effectiveTo" => "0001-01-01",
            ],
            "businessName" => [
                "organisationName" => "SONGCOLLAB",
                "effectiveFrom" => "2019-11-11",
            ],
        ]);

        $this->assertEquals(AbnLookupService::searchByAbn('89 097 827 122'), [
            "recordLastUpdatedDate" => "2002-05-01",
            "ABN" => [
                "identifierValue" => "89097827122",
                "isCurrentIndicator" => "Y",
                "replacedFrom" => "0001-01-01",
            ],
            "entityStatus" => [
                "entityStatusCode" => "Active",
                "effectiveFrom" => "2002-05-01",
                "effectiveTo" => "0001-01-01",
            ],
            "ASICNumber" => "097827122",
            "entityType" => [
                "entityTypeCode" => "PRV",
                "entityDescription" => "Australian Private Company",
            ],
            "goodsAndServicesTax" => [
                "effectiveFrom" => "2002-05-01",
                "effectiveTo" => "0001-01-01",
            ],
            "mainName" => [
                "organisationName" => "INVENTUA PTY LTD",
                "effectiveFrom" => "2002-05-01",
            ],
            "mainBusinessPhysicalAddress" => [
                "stateCode" => "VIC",
                "postcode" => "3199",
                "effectiveFrom" => "2002-05-01",
                "effectiveTo" => "0001-01-01",
            ],
        ]);

        try {
            AbnLookupService::searchByAbn('wrong abn');
        } catch (\SparkEleven\AbnLookup\Exceptions\AbnLookupException $e) {
            $this->assertEquals($e->error(), [
                'exceptionDescription' => 'Search text is not a valid ABN or ACN',
                'exceptionCode' => 'WEBSERVICES',
            ]);
        }
    }

    /**
     * Test searchByAsic method.
     */
    public function testSearchByAsic()
    {
        $this->useDefaultConfig($this->app);
        $this->resetService();

        $this->assertEquals(AbnLookupService::searchByAsic('165 912 970'), [
            "recordLastUpdatedDate" => "2020-03-20",
            "ABN" => [
                "identifierValue" => "15165912970",
                "isCurrentIndicator" => "Y",
                "replacedFrom" => "0001-01-01",
            ],
            "entityStatus" => [
                "entityStatusCode" => "Active",
                "effectiveFrom" => "2013-09-23",
                "effectiveTo" => "0001-01-01",
            ],
            "ASICNumber" => "165912970",
            "entityType" => [
                "entityTypeCode" => "PRV",
                "entityDescription" => "Australian Private Company",
            ],
            "mainName" => [
                "organisationName" => "MUSINDIE PTY. LTD.",
                "effectiveFrom" => "2013-09-23",
            ],
            "mainBusinessPhysicalAddress" => [
                "stateCode" => "VIC",
                "postcode" => "3103",
                "effectiveFrom" => "2017-02-27",
                "effectiveTo" => "0001-01-01",
            ],
            "businessName" => [
                "organisationName" => "SONGCOLLAB",
                "effectiveFrom" => "2019-11-11",
            ],
        ]);

        $this->assertEquals(AbnLookupService::searchByAsic('097 827 122'), [
            "recordLastUpdatedDate" => "2002-05-01",
            "ABN" => [
                "identifierValue" => "89097827122",
                "isCurrentIndicator" => "Y",
                "replacedFrom" => "0001-01-01",
            ],
            "entityStatus" => [
                "entityStatusCode" => "Active",
                "effectiveFrom" => "2002-05-01",
                "effectiveTo" => "0001-01-01",
            ],
            "ASICNumber" => "097827122",
            "entityType" => [
                "entityTypeCode" => "PRV",
                "entityDescription" => "Australian Private Company",
            ],
            "goodsAndServicesTax" => [
                "effectiveFrom" => "2002-05-01",
                "effectiveTo" => "0001-01-01",
            ],
            "mainName" => [
                "organisationName" => "INVENTUA PTY LTD",
                "effectiveFrom" => "2002-05-01",
            ],
            "mainBusinessPhysicalAddress" => [
                "stateCode" => "VIC",
                "postcode" => "3199",
                "effectiveFrom" => "2002-05-01",
                "effectiveTo" => "0001-01-01",
            ],
        ]);

        try {
            AbnLookupService::searchByAsic('wrong acn');
        } catch (\SparkEleven\AbnLookup\Exceptions\AbnLookupException $e) {
            $this->assertEquals($e->error(), [
                'exceptionDescription' => 'Search text is not a valid ABN or ACN',
                'exceptionCode' => 'WEBSERVICES',
            ]);
        }
    }

    /**
     * Test searchByName method.
     */
    public function testSearchByName()
    {
        $this->useDefaultConfig($this->app);
        $this->resetService();

        $this->assertEquals((AbnLookupService::searchByName('Spark Eleven'))['searchResultsRecord'][0], [
            "ABN" => [
                "identifierValue" => "41726564339",
                "identifierStatus" => "Active",
            ],
            "businessName" => [
                "organisationName" => "Spark Eleven",
                "score" => 100,
                "isCurrentIndicator" => "Y",
            ],
            "mainBusinessPhysicalAddress" => [
                "stateCode" => "VIC",
                "postcode" => "3199",
                "isCurrentIndicator" => "Y",
            ],
        ]);

        $this->assertEquals((AbnLookupService::searchByName('MUSINDIE'))['searchResultsRecord'][0], [
            "ABN" => [
                "identifierValue" => "15165912970",
                "identifierStatus" => "Active",
            ],
            "mainName" => [
                "organisationName" => "MUSINDIE PTY. LTD.",
                "score" => 100,
                "isCurrentIndicator" => "Y",
            ],
            "mainBusinessPhysicalAddress" => [
                "stateCode" => "VIC",
                "postcode" => "3103",
                "isCurrentIndicator" => "Y",
            ],
        ]);

        $this->assertEquals((AbnLookupService::searchByName('INVENTUA'))['searchResultsRecord'][0], [
            "ABN" => [
                "identifierValue" => "89097827122",
                "identifierStatus" => "Active",
            ],
            "mainName" => [
                "organisationName" => "INVENTUA PTY LTD",
                "score" => 100,
                "isCurrentIndicator" => "Y",
            ],
            "mainBusinessPhysicalAddress" => [
                "stateCode" => "VIC",
                "postcode" => "3199",
                "isCurrentIndicator" => "Y",
            ],
        ]);
    }

    /**
     * Test if an exception is thrown when no authentication GUID is provided.
     */
    public function testIfThrowExceptionWhenNoAuthGuidProvided()
    {
        $this->expectException(\SparkEleven\AbnLookup\Exceptions\NoAuthGuidException::class);

        $this->useDefaultConfig($this->app);
        $this->useAuthGuidNull($this->app);
        $this->resetService();

        AbnLookupService::searchByAbn('41 726 564 339');
    }

    /**
     * Test if an exception is thrown when wrong authentication GUID is provided.
     */
    public function testIfThrowExceptionWhenWrongAuthGuidProvided()
    {
        $isExceptionThrowed = false;

        $this->useDefaultConfig($this->app);
        $this->useAuthGuidIncorrect($this->app);
        $this->resetService();

        try {
            AbnLookupService::searchByAbn('41 726 564 339');
        } catch (\SparkEleven\AbnLookup\Exceptions\AbnLookupException $e) {
            $isExceptionThrowed = true;

            $this->assertEquals($e->error(), [
                'exceptionDescription' => 'The GUID entered is not recognised as a Registered Party',
                'exceptionCode' => 'WEBSERVICES',
            ]);
        }

        $this->assertTrue($isExceptionThrowed, 'AbnLookupException not thrown.');
    }

    /**
     * Test if an exception is thrown when wrong wsdl path is provided.
     */
    public function testIfThrowExceptionWhenWrongWsdlProvided()
    {
        $this->expectException(\SoapFault::class);

        $this->useDefaultConfig($this->app);
        $this->useWsdlIncorrect($this->app);
        $this->resetService();

        AbnLookupService::searchByAbn('41 726 564 339');
    }
}
