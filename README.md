sparkeleven/abn-lookup
======================

[![Latest Stable Version](https://poser.pugx.org/sparkeleven/abn-lookup/v)](//packagist.org/packages/sparkeleven/abn-lookup)
[![Total Downloads](https://poser.pugx.org/sparkeleven/abn-lookup/downloads)](//packagist.org/packages/sparkeleven/abn-lookup)
[![License](https://poser.pugx.org/sparkeleven/abn-lookup/license)](//packagist.org/packages/sparkeleven/abn-lookup)

`sparkeleven/abn-lookup` is a composer package for easy access to ABN Lookup SOAP API sets. Supports both **Laravel** `^5|^6|^7|^8` and also pure PHP/Composer project.

## Installation

### 1. Downloading package into your project

To plant this library on your Laravel project, you can simply run the following command inside your working directory of the Laravel project.

```shell
composer require sparkeleven/abn-lookup
```

### 2. Setting up service provider and alias

For Laravel versions earlier than *5.5* (which doesn't provide package auto-discovery), you should add the followings into your `config/app.php` file;

```php
// Under `providers` array
SparkEleven\AbnLookup\AbnLookupServiceProvider::class,

// Under `aliases` array
'AbnLookup' => SparkEleven\AbnLookup\Facades\AbnLookup::class,
```

### 3. Publishing the package into your project

Run the command below to publish the package (config, translations, views):

```shell
php artisan vendor:publish --provider=SparkEleven\\AbnLookup\\AbnLookupServiceProvider
```

## Configuration

After successfully published the package, you should check the `config/abn-lookup.php` file. Read the guides on this file to setup essential information (Authentication GUID and etc).

```php
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication GUID
    |--------------------------------------------------------------------------
    |
    | To access ABN Lookup web services, an authentication GUID (Globally
    |  Unique Identifier) is required. You can find your own GUID from the
    |  email received after your registration is completed.
    |
    | For more details, visit below link;
    |  https://abr.business.gov.au/Tools/WebServices#registration
    |
    */

    'auth_guid' => env('ABNLOOKUP_AUTH_GUID'),

    /*
    |--------------------------------------------------------------------------
    | WSDL path
    |--------------------------------------------------------------------------
    |
    | Path to WSDL which describes the web services of ABN Lookup. It is
    |  recommended not to change this value from default.
    |
    | For more details, visit below link;
    |  https://abr.business.gov.au/Tools/WebServices#wsdl
    |
    */

    'wsdl' => \SparkEleven\AbnLookup\Services\AbnLookupService::DEFAULT_WSDL,

    /*
    |--------------------------------------------------------------------------
    | WSDL cache method
    |--------------------------------------------------------------------------
    |
    | For better performance, it is recommended to cache the WSDL file. Do not
    |  change this value if you are not sure about it, or you will experience
    |  terrible response time.
    |
    | Must be one of;
    |  * WSDL_CACHE_NONE
    |  * WSDL_CACHE_DISK
    |  * WSDL_CACHE_MEMORY
    |  * WSDL_CACHE_BOTH
    |
    | For more details, visit below link;
    |  https://www.php.net/manual/en/soapclient.soapclient.php
    |
    */

    'wsdl_cache' => env('ABNLOOKUP_WSDL_CACHE', WSDL_CACHE_DISK),

];
```

## How to use

You can simply import `AbnLookup` facade to access static search methods provided.

Examples:

```php
use AbnLookup;

// searchByAbn
$result = AbnLookup::searchByAbn('33102417032');
echo $result['mainName']['organisationName']; // Google Australia Pty Ltd

// searchByAsic
$result = AbnLookup::searchByAsic('102417032');
echo $result['entityType']['entityDescription']; // Australian Private Company
echo $result['mainName']['organisationName']; // Google Australia Pty Ltd

// searchByName
$result = AbnLookup::searchByName('Google Australia Pty Ltd');
echo $result['searchResultsRecord'][0]['ABN']['identifierValue']; // 41726564339
```

You can check the correct i/o schema on the [WSDL document](https://abr.business.gov.au/abrxmlsearch/ABRXMLSearch.asmx?WSDL).

### Outside of Laravel

This package can be also used outside of Laravel project. Run below code snippet to initialize service class and use the same code as on Laravel project.

```php
use SparkEleven\AbnLookup\Services\AbnLookupService as AbnLookup;

// Initiate service
AbnLookup::reset(
    '00000000-0000-0000-0000-000000000000', // Your authentication GUID
    AbnLookup::DEFAULT_WSDL, // WSDL path
    WSDL_CACHE_DISK, // WSDL cache method
);
```

## License

`sparkeleven/abn-lookup` is free software distributed under the terms of the [**MIT license**](LICENSE).

## Contribution guidelines

Support follows PSR-1 and PSR-4 PHP coding standards, and semantic versioning.

Please report any issue you find in the issues page.
Pull requests are welcome.
